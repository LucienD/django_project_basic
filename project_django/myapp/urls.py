from django.urls import path
from .views import IndexView

# Comme expliqué dans le fichier général urls, ici nous sommes dans les sous
# urls de l'application, "app_name" permet de faire le liant avec le paramètre
# namespace de la function include vue en amont.
# 'name' fait suite au systeme de raccourci namespace.

# IndexView est le nom de la View qui sera utilisé pour gérer l'affichage du
# template et inséré de la logique si besoin.

app_name = 'myapp'
urlpatterns = [
    path('', IndexView.as_view(), name='index'),
]
