from django.views.generic import TemplateView
from .models import Message


# Voici une view basé sur une View generic Django, ce sont des class créé à
# la base par un externe au projet et cela a été incorporé dans la version
# 1.8, cela abstrait beaucoup de fonctionnalité, afin d'y voir plus clair,
# vous pouvez vous rendre sur ce lien qui detail chaque vue générique :
# https://ccbv.co.uk/

class IndexView(TemplateView):
    template_name = 'myapp/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['mes_messages'] = Message.objects.all()
        return context
