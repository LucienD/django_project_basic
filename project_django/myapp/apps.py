from django.apps import AppConfig


class MyappConfig(AppConfig):
    name = 'project_django.myapp'
