from django.contrib import admin
from .models import Message

# Register your models here.
# Django est livré avec un système d'administration et de session.
# Pour y accéder, il suffit de se rendre sur http://localhost:8000/admin.
# De base, les modèles de base de donnée que nous créons ne sont pas présente
# sur l'interface. Pour remédier à cela, Django met à disposition ce fichier.
# Il suffit d'écrire la ligne ci-dessous pour que notre modèle soit affiché.
# Bien d'autres options sont disponibles, allez voir la documentation à ce
# sujet : https://docs.djangoproject.com/fr/2.0/ref/contrib/admin/

admin.site.register(Message)
