"""project_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

# La configuration centralisée des urls, il faut ici créer les urls racines
# pour chaque application. include permet de faire le lien avec l'application
# le parametre "namespace" indique un raccourci d'url, c'est à dire que pour
# pointé vers l'url qui correspond à l'application il suffira de donner
# "myapp:nom_de_la_sous_url"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('myapp/', include('project_django.myapp.urls', namespace='myapp')),
]
